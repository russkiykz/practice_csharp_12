﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticeOfSystemCollections
{
    public class CollectionService
    {
        public static List<int> FillingCollection(List<int> collection, int count)
        {
            Random random = new Random();
            for (int i = 0; i < count; i++)
            {
                collection.Add(random.Next(0, 100));
            }
            return collection;
        }
        public static void PrintCollection(List<int> collection)
        {
            foreach (int element in collection)
            {
                Console.Write($"{ element} ");
            }
        }
        public static List<int> RemovingOddItems(List<int> collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                if (i % 2 != 0 && i != 0)
                {
                    collection[i] = -1;
                }
            }
            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i] == -1)
                {
                    collection.RemoveAt(i);
                }
            }
            return collection;
        }

        public static void ItemsMoreAvg(List<int> collection)
        {
            double sum = 0;
            foreach (int element in collection)
            {
                sum += element;
            }
            double average = sum / collection.Count;
            Console.WriteLine();
            foreach (int element in collection)
            {
                if (element > average)
                {
                    Console.Write($"{element} ");
                }
            }
        }
    }
}
