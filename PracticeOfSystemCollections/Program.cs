﻿using System;
using System.Collections.Generic;

namespace PracticeOfSystemCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<int> collection = new List<int>();
            CollectionService.FillingCollection(collection, 40);
            CollectionService.PrintCollection(collection);
            CollectionService.RemovingOddItems(collection);
            CollectionService.ItemsMoreAvg(collection);

            MyCollection<string,List<string>> myCollection = new MyCollection<string, List<string>>();       
            
        }
    }
}
